package com.inggs.demo.web;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class DemoController {

    @GetMapping(value = "/hello/{name}", produces = MediaType.TEXT_PLAIN_VALUE)
    String getHello(@PathVariable final String name) {
        return "Hello " + name + '!';
    }
}
